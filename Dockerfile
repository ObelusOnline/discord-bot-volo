FROM python:3.11
LABEL authors="Kevin Olynyk"

RUN DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get upgrade -y && apt-get clean;

COPY requirements.txt /
COPY src /srv/app

RUN python -m pip install --upgrade pip;
RUN python -m pip install -r requirements.txt

ENTRYPOINT ["python", "/srv/app/main.py"]