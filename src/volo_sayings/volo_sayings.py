import re
from random import randint
from os import path
import logging

# log_level = logging.INFO
#
# logging.basicConfig(
#                     level=log_level
#                     )
#
# log = logging.getLogger()
#
# handler = logging.StreamHandler()
#
# formatter = logging.Formatter('time=\'%(asctime)s\', name=\'%(name)s\', level=\'%(levelname)s\', message=\'%('
#                               'message)s\'')
#
# handler.setFormatter(formatter)
#
# log.addHandler(handler)


class VoloSayings(object):
    def __init__(self, char_name, log) -> None:
        self.log = log
        self.char_name = char_name
        self.think_says = self.__load_say_list(self, 'thinking.txt', char_name)
        self.forget_says = self.__load_say_list(self, 'forgetting.txt', char_name)

    @staticmethod
    def __load_say_list(self, file_name, char_name='Bot'):

        # get the absolute path of the module's directory
        module_dir = path.dirname(path.abspath(__file__))

        # specify the relative path to the file from the module's directory
        file_rel_path = file_name

        self.log.info(f"finding file {file_name} in path {module_dir}")

        # join the paths together
        self.think_path = path.join(module_dir, file_rel_path)

        with open(self.think_path, 'r', encoding='utf-8') as f:
            line_list = []
            for line in f.readlines():
                append_line = re.sub('{{Char}}', char_name, line)
                append_line = re.sub('\n', '', append_line)
                line_list.append(append_line)
        return line_list

    def get_think_says(self) -> str:
        return self.think_says[randint(0, len(self.think_says) - 1)]

    def get_forget_says(self) -> str:
        return self.forget_says[randint(0, len(self.forget_says) - 1)]
