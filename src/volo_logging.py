import logging
import traceback
from functools import wraps

# Set up basic logging to file 'error_log.txt'. This is the global error handler.
logging.basicConfig(filename="error_log.txt", level=logging.ERROR)


async def log_traceback(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            logging.error("There was an exception in  " + fn.__name__)
            logging.error(traceback.format_exc())
    return wrapper
