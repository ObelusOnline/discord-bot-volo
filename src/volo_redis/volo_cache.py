import redis.asyncio as redis


class RedisAsync:
    """Implement a simple redis client for the volo bot
    that can read write to a redis database.

    :param: host: redis host
    :param: port: redis port
    """

    def __init__(self, host, port, db, log):
        self.port = port
        self.host = host
        self.db = int(db)
        self.log = log
        self.log.info("connecting to redis.")
        self.redis = redis.Redis(host=self.host, port=self.port, db=self.db,
                                 socket_timeout=2, socket_connect_timeout=2)

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()

    async def __aenter__(self):
        await self.connect()

    async def close(self):
        if self.redis:
            await self.redis.aclose()

    async def connect(self):
        self.redis = await redis.Redis(host=self.host, port=self.port, db=self.db,
                                       socket_timeout=2, socket_connect_timeout=2)
        # except TimeoutError as e:
        #     self.log.fatal('timeout connecting to redis.')
        #     exit(0)

    async def save_prompt(self, key, prompt_input):
        _r = self.redis
        async with _r.pipeline(transaction=True) as pipe:
            return await pipe.json().set(key, '$', prompt_input).execute()

    async def load_prompt(self, key):
        _r = self.redis
        async with _r.pipeline(transaction=True) as pipe:
            return await pipe.json().get(key).execute()

    async def delete_prompt(self, key):
        _r = self.redis
        async with _r.pipeline(transaction=False) as pipe:
            return await pipe.delete(key).execute()

# # Example usage
# async def main():
#     r = RedisAsync()
#     await r.connect()
#     huh = await r.save_prompt(prompt)
#     print(huh)
#
# # Run the event loop
# asyncio.run(main())
