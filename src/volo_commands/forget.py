from discord.ext import commands


class Forget(commands.Cog):
    def __init__(self, bot, redis, sayings, log):
        self.log = log
        self.bot = bot
        self.redis = redis
        self.sayings = sayings
        self.log.info(f"Loaded module {__name__}")

    @commands.command(name="forget")
    async def forget(self, ctx):
        mention_user = ctx.author.name
        self.log.info(f'forgetting messages from {mention_user}')
        say = self.sayings.get_forget_says()
        await ctx.channel.send(f'*{say}*')
        await self.redis.connect()
        await self.redis.delete_prompt(f'chat:{mention_user}')
        await self.redis.close()
