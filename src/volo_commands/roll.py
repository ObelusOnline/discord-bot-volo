from discord.ext import commands
import random


class Roll(commands.Cog):
    def __init__(self, bot, log):
        self.log = log
        self.bot = bot
        self.log.info(f"Loaded module {__name__}")

    @commands.command(name="roll")
    async def roll(self, ctx, dice: str):
        """
        Rolls n dice in NdN format.
        """
        try:
            rolls, limit = map(int, dice.split('d'))
            if rolls > 100:
                raise ValueError("Value is too big!")
        except ValueError as e:
            await ctx.send('It would be a grave mistake for me to case more than 100 dice!')
            return
        except Exception:
            await ctx.send('Format has to be in NdN!')
            return
        result = "Your rolls my fine fellow:\n"
        result += ', '.join(str(random.randint(1, limit)) for _ in range(rolls))
        await ctx.send(result)