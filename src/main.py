import asyncio
import logging
import os
import discord

from discord.ext import commands
from dotenv import load_dotenv
from volo_commands.forget import Forget
from volo_commands.roll import Roll
from volo_events.on_ready import OnReady
from volo_events.on_message import OnMessage
from volo_redis import volo_cache
from volo_sayings import volo_sayings

logging.root.setLevel(logging.FATAL)

discord_log = logging.getLogger('discord')
discord_log.setLevel(logging.FATAL)

# Optionally, configure other loggers related to discord.py, such as websockets
websockets_logger = logging.getLogger('websockets')
websockets_logger.setLevel(logging.FATAL)

log_level = logging.INFO

log = logging.getLogger(__name__)
log.setLevel(log_level)
handler = logging.StreamHandler()

formatter = logging.Formatter('time=\'%(asctime)s\', name=\'%(name)s\', level=\'%(levelname)s\', message=\'%('
                              'message)s\'', "%Y-%m-%d %H:%M:%S")

handler.setFormatter(formatter)

log.addHandler(handler)

if load_dotenv():
    log.warning("Loading .env file")
else:
    log.warning("No .env file found, using environment.")

TOKEN = os.getenv('DISCORD_TOKEN')

GUILD = os.getenv('DISCORD_GUILD')

COMMAND_STRING = os.getenv('COMMAND_STRING', ';;')

LLM_ENDPOINT = os.getenv('LLM_ENDPOINT', 'http://0.0.0.0/nothing')
log.warning(f"Using LLM endpoint {LLM_ENDPOINT}")

LLM_MEMORY = int(os.getenv('LLM_MEMORY', 10))

REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')
REDIS_PORT = os.getenv('REDIS_PORT', '6379')
REDIS_DB = os.getenv('REDIS_DB', 0)
log.warning(f"Using Redis host {REDIS_HOST:s}:{REDIS_PORT:s}/{REDIS_DB}")


DEBUG = int(os.getenv('DEBUG', 0))

if DEBUG == 0:
    log.warning("Debug mode is off")
else:
    logging.basicConfig(level=logging.DEBUG)

    log.debug("Debug mode in ON")
    discord_log.setLevel(logging.INFO)
    websockets_logger.setLevel(logging.INFO)

intents = discord.Intents.default()

intents.message_content = True
intents.messages = True
intents.members = True

bot = commands.Bot(command_prefix=commands.when_mentioned_or(COMMAND_STRING),
                   intents=intents)

redis = volo_cache.RedisAsync(REDIS_HOST,
                              REDIS_PORT,
                              REDIS_DB,
                              log)

redis.connect()

sayings = volo_sayings.VoloSayings('Volo', log)


def debug_message(message: str) -> str:
    if DEBUG == 1:
        print(f'[DEBUG] {message}')
        return f"[DEBUG] {message}"
    else:
        pass


async def __add_cogs(bot_module: commands.Bot):
    await bot_module.add_cog(OnReady(bot, log))
    await bot_module.add_cog(OnMessage(bot,
                                       redis,
                                       sayings,
                                       LLM_ENDPOINT,
                                       LLM_MEMORY,
                                       COMMAND_STRING,
                                       log
                                       ))

    await bot_module.add_cog(Forget(bot, redis, sayings, log))
    await bot_module.add_cog(Roll(bot, log))


if __name__ == '__main__':
    # We need to do an async run here in order to add all the commands to the bot.
    asyncio.run(__add_cogs(bot))

    # This is the asynchronous run of the bot.
    bot.run(token=TOKEN,
            log_handler=None)

    exit(0)
