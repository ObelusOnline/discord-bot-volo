import logging
import re
import aiohttp
import json
from discord.ext import commands

init_prompt = {
    "messages": [
        # {
        #     "role": "user",
        #     "content": f""
        # },
    ],
    "mode": "chat",
    "character": "Volo"
}


class OnMessage(commands.Cog):
    def __init__(self, bot, redis, sayings, llm_endpoint, llm_memory, command_string, log: logging.Logger):
        self.llm_endpoint = llm_endpoint
        self.llm_memory = llm_memory
        self.sayings = sayings
        self.redis = redis
        self.bot = bot
        self.command_string = command_string
        self.log = log
        self.prohibited_chars = '[^a-zA-Z0-9 *,\'\"]+'
        self.log.info(f"Loaded event handler {__name__}")

    @staticmethod
    async def __get_text(self, ai_prompt: dict):
        async with aiohttp.ClientSession() as session:
            async with session.post(url=self.llm_endpoint,
                                    data=json.dumps(ai_prompt),
                                    headers={"Content-Type": "application/json"}
                                    ) as resp:
                self.log.info(f'ai server response code: {resp.status}')
                if resp.status != 200:
                    return None
                else:
                    return await resp.json()

    @commands.Cog.listener()
    async def on_message(self, message):

        if message.content.startswith(";;"):
            await self.bot.process_commands(message)
            return

        self.log.debug(f"Received message: {message.content}")

        # do nothing when a message is sent from the bot.
        # we don't want volo talking to himself...
        if message.author == self.bot.user:
            return

        # additionally, skip all messages starting with the command sequence
        # this prevents doubling the messages on commands.
        if message.content.startswith(self.command_string):
            return

        self.log.info(f"new message seen.")

        if message.author.bot is False:
            # check if the mention is blank or has text following.
            if len(self.bot.user.mention) < len(message.content):
                self.log.info(f"bot is mentioned.")

                # get the username who @ mentioned the bot.
                mention_user = message.author

                # provide a canned message to let the user know that the bot is 'thinking'
                say = self.sayings.get_think_says()
                await message.channel.send(f"*{say}*")

                # get the users message and trim the discord id <@1231231231123> off of it.
                # additionally trim off any special characters except * , . " '
                user_message = re.sub(self.prohibited_chars, "", message.content.split(">", 1)[1])

                self.log.info(f'{mention_user} requested prompt: {user_message}')

                # get the users prompt from the redis cache. If there is no prompt available,
                # initialize the prompt with a pre-made dict.
                prompt = await self.redis.load_prompt(f'chat:{mention_user}')

                # redis returns the prompt in an array, so we need to access the 0th element
                # here to make sure we're getting the dict at the level we need to manipulate it.
                if prompt[0] is not None:
                    prompt = prompt[0]
                else:
                    prompt = init_prompt

                prompt['messages'].append(
                    {
                        "role": "user",
                        "content": user_message
                    }
                )

                # save the users prompt to a cache with their name
                await self.redis.save_prompt(f'chat:{mention_user}', prompt)

                response = await self.__get_text(self, prompt)
                response = response.get('choices')[0].get("message").get("content")

                stored_msg = await self.redis.load_prompt(f'chat:{mention_user}')

                if stored_msg[0] is not None:
                    self.log.info(f'memory cache hit!')
                    # debug_message('printing memory contents')
                    # debug_message(stored_msg[0])
                    prompt = stored_msg[0]

                prompt['messages'].append(
                    {
                        "role": "assistant",
                        "content": response
                    }
                )

                # if we have over LLM_MEMORY messages, start slicing the first one off of the stack.
                # until we have fewer than that so that we don't overwhelm the server.
                # this will reduce the capability to keep a conversation going on the same
                # topic for very long.
                while len(prompt["messages"]) > self.llm_memory:
                    prompt["messages"] = prompt["messages"][1:]

                await self.redis.connect()
                self.log.info(f"writing to cache.")
                await self.redis.save_prompt(f'chat:{mention_user}', prompt)

                await self.redis.close()

                await message.channel.send(f'{response}')

            else:
                if not self.bot.user.mention:
                    await message.channel.send(
                        f'Ah! Friend, you can ask me a question directly, or type ;; for a demonstration '
                        f'of my spells')

