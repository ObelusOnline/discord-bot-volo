from discord.ext import commands


class OnReady(commands.Cog):
    def __init__(self, bot, log):
        self.bot = bot
        self.log = log
        self.log.info(f"Loaded event handler {__name__}")

    @commands.Cog.listener()
    async def on_ready(self):
        self.log.info(f'logged in as {self.bot.user}')
        for guild in self.bot.guilds:
            self.log.info(f'{self.bot.user} is a member of {guild}')
